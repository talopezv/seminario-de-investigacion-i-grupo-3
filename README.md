
Universidad Nacional Mayor de San Marcos / Maestría de Ingeniería de Sistemas e Informática con Mención en Ingeniería de Software

## DISEÑO DE UN SISTEMA GENERADOR DE APLICACIONES IOT ORIENTADAS AL APRENDIZAJE AUTOMÁTICO USANDO SENSORTHINGS API E INGENERÍA DE DOMINIO
- ODS 9: Industria, Innovación e Infraestructura
- PROYECTO DE TESIS COLABORATIVO

# Datos del Proyecto
- Curso: Seminario de la Investigación I
- Docente: Dr. Iván Encalada Díaz
- Ciclo: 2022-II

## Grupo de Trabajo
Grupo 3: 
- López Villanueva, Timoteo Andrés
- Rodríguez Rivera, Francisco Marcos
- Vega Blas, Robert Jaime
